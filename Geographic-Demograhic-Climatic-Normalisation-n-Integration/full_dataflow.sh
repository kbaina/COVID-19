#!/bin/sh

#Author : Karim Baïna, ENSIAS, Mohammed V University
#Research Team : Alqualsadi research team (Innovation on Digital and Enterprise Architectures)
#                ADMIR Laboratory, Rabat IT Center,
#Institution : ENSIAS, University Mohammed V in Rabat,
#              BP 713 Agdal, Rabat, Morocco
#e-mail : karim.baina@um5.ac.ma, karim.baina@gmail.com
#twitter : @kbaina
#Date : April 2nd, 2020; June 5th, 2020
#http://ensias.um5.ac.ma/professor/m-karim-baina
#https://scholar.google.com/citations?user=tRtXdkEAAAAJ&hl=fr
#http://www.slideshare.net/kbaina
#https://gitlab.com/kbaina

if [ $# -eq 0 ]
then
  echo "syntax ./$0 recovered | confirmed | deaths"
  exit
else
  case "$1" in
   "recovered" | "confirmed" | "deaths")
    specific=$1
    ;;
   *)
    echo "syntax ./$0 recovered | confirmed | deaths"
    exit
    ;;
esac
fi

# pre-processing : normalisation
./normalisation_flow.sh ${specific}

# processing : integration
./integration_flow.py ${specific} > ./output_data/time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv

# post-processing :
head -n 1 ./output_data/time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv > ./output_data/time_series_covid19_${specific}_formatted-sparse-us2-1-normalisation-with-full-integration.csv 
./days_bis.sh `head -n 1 ./output_data/time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv` > ./output_data/time_series_covid19_${specific}_formatted-sparse-shifted-to-D0-us3-normalisation-with-full-integration.csv
tail -n +2 ./output_data/time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv |
sed "s/, 0,/,,/g"  |
sed  -E "s/([,]+) 0,/\1,/g" |
sed -E "s/, 0($)/,\1/" | tee ./output_data/time_series_covid19_${specific}_formatted-sparse.tmp.csv |
sed -E "s/([,]+)((,[ 0-9]+)((,[ 0-9]*)*))/\2\1/" >> ./output_data/time_series_covid19_${specific}_formatted-sparse-shifted-to-D0-us3-normalisation-with-full-integration.csv
cat ./output_data/time_series_covid19_${specific}_formatted-sparse.tmp.csv >>  ./output_data/time_series_covid19_${specific}_formatted-sparse-us2-1-normalisation-with-full-integration.csv 


# purge : let only chosen output files according to my normalisation/integration user stories preference
#time_series_covid19_recovered_global-us1-normalisation.csv
rm ./output_data/time_series_covid19_${specific}_global-us1-normalisation.csv
#time_series_covid19_recovered_global-sparse-with-formatted-column-names-us2-1-normalisation.csv
rm ./output_data/time_series_covid19_${specific}_global-sparse-with-formatted-column-names-us2-1-normalisation.csv
#time_series_covid19_${specific}_global-sparse-us2-2-normalisation.csv
rm ./output_data/time_series_covid19_${specific}_global-sparse-us2-2-normalisation.csv
#time_series_covid19_recovered_global-sparse-shifted-to-D0-us3-normalisation.csv
rm ./output_data/time_series_covid19_${specific}_global-sparse-shifted-to-D0-us3-normalisation.csv
#time_series_covid19_recovered_formatted-sparse.tmp.csv
rm ./output_data/time_series_covid19_${specific}_formatted-sparse.tmp.csv
#time_series_covid19_recovered_formatted-us1-normalisation-with-full-integration.csv
rm ./output_data/time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv



#./V${version}/generic-mapper-delta-print.py ${specific} > time_series_covid19_${specific}_formatted-delta-print.csv
#head -n 1 time_series_covid19_${specific}_formatted-us1-normalisation-with-full-integration.csv > time_series_covid19_${specific}_formatted-sparse-delta-print.csv 
#./V${version}/days_bis.sh `head -n 1 time_series_covid19_${specific}_formatted-delta-print.csv |
#sed "s/,/\t/g"` > time_series_covid19_${specific}_formatted-sparse-shifted-to-D0-delta-print.csv
#tail -n +2 time_series_covid19_${specific}_formatted-delta-print.csv |
#sed "s/, 0,/,,/g"  |
#sed  -E "s/([,]+) 0,/\1,/g" |
#sed -E "s/, 0($)/,\1/" | tee time_series_covid19_${specific}_formatted-sparse.tmp-delta-print.csv |
#sed -E "s/([,]+)((,[ 0-9]+)+)/\2\1/" >> time_series_covid19_${specific}_formatted-sparse-shifted-to-D0-delta-print.csv
#cat time_series_covid19_${specific}_formatted-sparse.tmp.csv >>  time_series_covid19_${specific}_formatted-sparse-delta-print.csv
#rm time_series_covid19_${specific}_formatted-sparse.tmp-delta-print.csv
#rm time_series_covid19_${specific}_formatted-delta-print.csv
#rm time_series_covid19_${specific}_global-tmp-delta-print.csv
#rm time_series_covid19_${specific}_global-sparse-with_formatted-column-names-delta-print.csv
#rm time_series_covid19_${specific}_global-sparse-shifted-to-D0-delta-print.csv
#rm time_series_covid19_${specific}_global-sparse-delta-print.csv
