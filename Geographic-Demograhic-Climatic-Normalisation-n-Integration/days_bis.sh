#!/bin/sh

#Author : Karim Baïna, ENSIAS, Mohammed V University
#Research Team : Alqualsadi research team (Innovation on Digital and Enterprise Architectures)
#                ADMIR Laboratory, Rabat IT Center,
#Institution : ENSIAS, University Mohammed V in Rabat,
#              BP 713 Agdal, Rabat, Morocco
#e-mail : karim.baina@um5.ac.ma, karim.baina@gmail.com
#twitter : @kbaina
#Date : April 4th, 2020; June 5th, 2020
#http://ensias.um5.ac.ma/professor/m-karim-baina
#https://scholar.google.com/citations?user=tRtXdkEAAAAJ&hl=fr
#http://www.slideshare.net/kbaina
#https://gitlab.com/kbaina


titles=$*
columns=`echo "$titles" | sed "s/Country\/Region,[ ]//" | sed -E "s/[0-9]{2}\/[0-9]{2}\/[0-9]{4},[ ]//g"`

#columns will store columns names after Country name & dates (Country/Region, 01/22/2020, 01/23/2020, .., 06/05/2020)

columnsnumber=`echo "$columns" | grep "," | wc -w`

suite1=""
bornesup1=`expr $# - $columnsnumber - 2`
for i in $(seq 0 $bornesup1) ; do
  suite1="$suite1,D$i"
done

suite2=","$columns

title1=`echo $1 | sed "s/,//"`
echo "$title1$suite1$suite2"
