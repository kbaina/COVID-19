#!/bin/sh

#Author : Karim Baïna, ENSIAS, Mohammed V University
#Research Team : Alqualsadi research team (Innovation on Digital and Enterprise Architectures)
#                ADMIR Laboratory, Rabat IT Center,
#Institution : ENSIAS, University Mohammed V in Rabat,
#              BP 713 Agdal, Rabat, Morocco
#e-mail : karim.baina@um5.ac.ma, karim.baina@gmail.com
#twitter : @kbaina
#Date : March 31st, 2020
#http://ensias.um5.ac.ma/professor/m-karim-baina
#https://scholar.google.com/citations?user=tRtXdkEAAAAJ&hl=fr
#http://www.slideshare.net/kbaina
#https://gitlab.com/kbaina

specific=$1

#$1 script parameter may be 'confimed' or 'deaths' or 'recovered' 

sed "s/, /-/" ./COVID-19/csse_covid_19_data/csse_covid_19_time_series/
time_series_covid19_${specific}_global.csv | sed "s/\"//g" |
sed "s/\*//" | sed -E "s/\,(.)\//,0\1\//g" |
sed -E "s/\/(.)\//\/0\1\//g" |
sed -E "s/\/20([^/])/\/2020\1/g" |
sed -E "s/\/20$/\/2020/g"| sed -E "s/,($)/,0\1/g" |
sed "s/,0,/,,/g"| sed  -E "s/([,]+)0,/\1,/g" |
sed -E "s/,0($)/,\1/" | | sed  "s/^,/~/" |
sed -E "s/([a-z A-Z]+),([a-z A-Z]+)/\1~\2/" > 
time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv


# time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv
# contains date formatted columns useful for any further date arithmetics and manipulation (e.g. duration calculations, D0 of COVID-19, D0 of n^th death manipulation, etc.)


tail -n +2 time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv > time_series_covid19_${specific}_global-sparse.csv
